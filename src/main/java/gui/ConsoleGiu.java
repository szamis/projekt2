package gui;

import dao.impl.DVDDaoImpl;
import model.DVD;
import service.DVDService;

import java.util.Scanner;

public class ConsoleGiu implements Gui {

    private Scanner scanner = new Scanner(System.in);
    private DVDService dvdService = new DVDService(new DVDDaoImpl());

    @Override
    public void mainMenu() {
        Integer menuChoice = null;

        Integer film = null;
        do {
            printMenuOptions();
            menuChoice = getInt();

            switch (menuChoice) {
                case 0:
                    System.out.println("Bye bye");
                    break;
                case 1:
                    dvdService.getAllAviableDVDs().forEach(System.out::println);
                    break;
                case 2:
                    filterMenu();
                    break;
                case 3:
                    System.out.println("Podaj id wybranego filmu");
                    film = getInt();
                    dvdService.lendDVD(film);
                    break;
                case 4:
                    System.out.println("Podaj id filmu ktory chcesz oddac");
                    film = getInt();
                    Integer finalFilm = film;
                    DVD dvd = dvdService.getAll().stream().filter(e -> e.getId() == finalFilm).findAny().get();
                    System.out.println("do zapłaty: " + dvdService.calculatePay(dvd)+"$");
                    dvdService.returnDVD(film);
                    break;
                case 5:
                    printAdingMenu();
                    break;
                default:
                    System.out.println("Niepoprawny wobor sprobuj ponownie ");
            }

        } while (menuChoice != 0);
    }


    private void printAdingMenu() {
        System.out.println("Podaj tytuł");
        String title = scanner.nextLine();
        System.out.println("Podaj rok");
        int year = getInt();
        System.out.println("Podaj rezsyera");
        String director = scanner.nextLine();
        System.out.println("Podaj cene");
        int price = getInt();
        int max = dvdService.getAll().stream().map(e -> e.getId()).mapToInt(e -> e)
                .max().orElse(0);
        dvdService.addDVD(new DVD(max + 1, title, year, director, true, price));
    }


    private void printMenuOptions() {
        System.out.println("0 - Wyjscie");
        System.out.println("1 - Wyswietl dostepne filmy");
        System.out.println("2 - wyswietl filmy po");
        System.out.println("3 - Wypozycz film");
        System.out.println("4 - Oddaj film");
        System.out.println("5 - Dodawanie nowego filmu do wypozyczalni");
    }

    private Integer getInt() {
        String s = scanner.nextLine();
        return Integer.parseInt(s);
    }

    private void printFilterMenu() {
        System.out.println("0 - Wyjscie");
        System.out.println("1 - tytule");
        System.out.println("2 - roku");
        System.out.println("3 - rezyserze");
        System.out.println("4 - cena");
    }

    private void filterMenu() {
        Integer filterChoice = null;
        do {
            printFilterMenu();
            filterChoice = getInt();

            switch (filterChoice) {
                case 0:
                    System.out.println("Bye");
                    break;
                case 1:
                    dvdService.getAllByTitle().forEach(System.out::println);
                    break;
                case 2:
                    dvdService.getAllByCustom(DVD.YEAR_COMPARATOR).forEach(System.out::println);
                    break;
                case 3:
                    dvdService.getAllByCustom(DVD.DIRECTOR_COMPARATOR).forEach(System.out::println);
                    break;
                case 4:
                    dvdService.getAllByCustom(DVD.PRICE_COMPARATOR).forEach(System.out::println);
                    break;

                default:
                    System.out.println("Niepoprawny wobor sprobuj ponownie ");
            }
        } while (filterChoice != 0);
    }


}
