package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class DVD implements Serializable {
    public DVD(int id, String title, int year, String director, boolean isAvaliable, int price) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.director = director;
        this.isAvaliable = isAvaliable;
        this.price = price;
    }

    private int id;
    private String title;
    private int year;
    private String director;
    private boolean isAvaliable;
    private int price;
    private LocalDate lendingDay;

    public void setLendingDay(LocalDate now) {
        this.lendingDay = now;
    }

    public static final Comparator<DVD> TITLE_COMPARATOR = Comparator.comparing(DVD::getTitle);
    public static final Comparator<DVD> DIRECTOR_COMPARATOR = (x, y) -> x.getDirector().compareTo(y.getDirector());
    public static final Comparator<DVD> YEAR_COMPARATOR = (x, y) ->  x.getYear() - y.getYear();
    public static final Comparator<DVD> PRICE_COMPARATOR = Comparator.comparing(DVD::getPrice);
}
