package service;

import dao.DVDDao;
import model.DVD;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class DVDService {
    private DVDDao dvdDao;

    public DVDService(DVDDao dvdDao) {
        this.dvdDao = dvdDao;
        dvdDao.load();
    }

    public List<DVD> getAll() {
        return dvdDao.getAll();
    }

    public List<DVD> getAllAviableDVDs() {
        return dvdDao.getAllBy(DVD::isAvaliable);
    }

    public List<DVD> getAllByTitle() {
        return dvdDao.getAllSortBy(DVD.DIRECTOR_COMPARATOR);
    }

    public List<DVD> getAllByCustom(Comparator comparator) {
        return dvdDao.getAllSortBy(comparator);
    }

    public void addDVD(DVD dvd) {
        dvdDao.add(dvd);
        dvdDao.save();
    }

    public void lendDVD(int id) {
        Optional<DVD> any = getAllAviableDVDs().stream().filter(e -> e.getId() == id).findAny();
        any.get().setAvaliable(false);
        any.get().setLendingDay(LocalDate.now());
        dvdDao.save();
    }

    public void returnDVD(int id) {
        Optional<DVD> any = getAll().stream().filter(e -> e.getId() == id).findAny();
        any.get().setAvaliable(true);
        any.get().setLendingDay(null);
        dvdDao.save();
    }

    public long calculatePay(DVD dvd) {
        LocalDate now = LocalDate.now();
        final long l = ChronoUnit.DAYS.between(dvd.getLendingDay(), now) * dvd.getPrice() + 2;
        return l;

    }


}
