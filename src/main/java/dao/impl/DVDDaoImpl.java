package dao.impl;

import dao.DVDDao;
import model.DVD;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class DVDDaoImpl implements DVDDao {

    private ArrayList<DVD> dvdList;
    private Path path = Paths.get("dvdList.ser");

    public DVDDaoImpl() {
        this.dvdList = new ArrayList<>();
    }


    @Override
    public List<DVD> getAll() {
        return new ArrayList<>(dvdList);
    }

    @Override
    public List<DVD> getAllSortBy(Comparator<DVD> comparator) {
        return dvdList.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<DVD> getAllBy(Predicate<DVD> DVDPredicate) {
        return dvdList.stream()
                .filter(DVDPredicate)
                .collect(Collectors.toList());
    }

    @Override
    public void add(DVD dvd) {
        this.dvdList.add(dvd);
    }

    @Override
    public void save() {
        try (OutputStream outputStream = Files.newOutputStream(path);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(dvdList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load() {
        try (InputStream inputStream = Files.newInputStream(path);
             ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)
        ) {
            this.dvdList = (ArrayList<DVD>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Brak filmow");
        }


    }

}
