package dao;

import model.DVD;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public interface DVDDao {

    List<DVD> getAll();

    List<DVD> getAllSortBy(Comparator<DVD> comparator);

    List<DVD> getAllBy(Predicate<DVD> DVDPredicate);

    void add(DVD dvd);

    void save();

    void load();


}
